import React from 'react'
import { Card, Image } from 'semantic-ui-react'

const CardExampleCard = () => (
  <Card>
    <Image src={this.props.photo}/>
    <Card.Content>
      <Card.Header>{this.props.name}</Card.Header>
      <Card.Meta>{this.props.bio}</Card.Meta>
      <Card.Description>{this.props.type}</Card.Description>
    </Card.Content>
  </Card>
)

export default CardExampleCard